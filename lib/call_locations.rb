require 'collabda'

Collabda.collection(:CallLocations) do
  source "data/zoom-links.yml", type: :yaml
  properties :meeting

  def link
    "https://gitlab.zoom.us/j/#{@meeting}"
  end
end
