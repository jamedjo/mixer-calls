require './lib/call_locations'

class GroupCollection
  include Enumerable

  GROUP_SIZE = 9
  DEFAULT_LOCATION = './data/groups.yml'

  attr_reader :members

  def initialize(members)
    @members = members
    @groupings = members.all.shuffle.each_slice(GROUP_SIZE).to_a
  end

  def groups
    @groupings.zip(CallLocations).map do |group_members, call_location|
      Group.new(group_members, call_location)
    end
  end

  def each(&block)
    groups.each(&block)
  end

  def save(location: DEFAULT_LOCATION)
    File.open(location, 'w') do |f|
      YAML.dump(groups, f)
    end
  end

  def self.load(location: DEFAULT_LOCATION)
    File.open(location) do |f|
      YAML.load(f)
    end
  end

  class Group
    attr_reader :members, :call_location

    def initialize(members, call_location)
      @members = members
      @call_location = call_location
    end

    def link
      call_location.link
    end
  end
end
