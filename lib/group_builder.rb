require_relative './team_fetcher'
require_relative './group_collection'

class GroupBuilder
  def self.execute
    TeamFetcher.remote_fetch
    team_members = TeamMembers.from_yaml
    GroupCollection.new(team_members).save
  end
end
