class NanocGroupSource < ::Nanoc::DataSource
  identifier :groups

  def up
    @groups = GroupCollection.load
  end

  def items
    @groups.map.with_index do |group, id|
      new_item(group, {}, "/#{id}")
    end
  end
end
