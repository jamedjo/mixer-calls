require 'faraday'

class TeamFetcher
  REMOTE_DATA_URL = 'https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/team.yml'

  def self.remote_fetch
    update_data
    local_load
  end

  def self.local_load
    require_relative './team_members'
    TeamMembers.from_yaml
  end

  def self.update_data
    team_yml = Faraday.get(REMOTE_DATA_URL).body
    File.write('./data/team.yml', team_yml)
  end
end
