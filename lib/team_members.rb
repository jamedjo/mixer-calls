require 'collabda'

class TeamMembers
  include Collabda

  @source_path = "data/team.yml"
  @format = :yaml

  properties :name, :placeholder, :locality, :country, :twitter, :gitlab

  attr_reader :name, :placeholder

  def self.all
    super.reject(&:placeholder)
  end

  def self.from_yaml
    self.build_collection
    self
  end
end
