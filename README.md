## Aim

To get everone at GitLab interacting.

When we meet at the Summits we get a lot of value from interacting in small groups.
Social interaction helps us get to know each other better, meeting team mates in
person helps us work through problems, and cross functional conversations break down silos.

In the 9 months between we miss a lot of this. Giving a presentation on the team
call isn't quite the same, with the interaction normally going one way aside from the chat.
The calls have also got much longer as we grow making it harder to concentrate
through to the end and shifting the conversation towards bigger updates.
Meetups like those we have in London are amazing but can't be attended by everyone.

In https://gitlab.com/gitlab-com/organization/issues/133 it was suggested we
break out into small randomly generated groups. This could help us bring more
interaction back into calls, keep them engaging and allow spontanious conversations
to evolve.

This project is to help us automate that, and explore potential improvements.

See https://gitlab.com/gitlab-com/organization/issues/133, https://gitlab.com/gitlab-com/people-ops/General/issues/40 and https://gitlab.com/gitlab-com/people-ops/General/issues/82
